# Feedback to Learner
19/12/22 13:56
Deductions:

Part 1: Wireshark

-        Remote MAC address not identified. - 5 marks

-        Absence of actual remote MAC address not explained. - 10 marks

Part 2: Small Office Network

-        WPA2 Enterprise & AAA not used. - 4 marks

Part 3: DNS and HTTP Servers

-        Using “ping” for DNS resolution tests. - 3 marks

Bonus:

-        Bonus not implemented. - 5 marks

The report is well formatted despite a few mistakes in configurations and testing.

